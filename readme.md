Most style changes should go in the stylesheet.

The JavaScript will all be put into the canvas tags
in index.html

Once we get sketches of the game items, we can scan them 
if necessary and store them under reference/sketches

For testing, open index.html in Chrome or Firefox