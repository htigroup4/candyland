// contains functions used by the candyland game

// onload behavior
// if a form has been submitted, the names will be 
// read from the query string. This will result in
// two changes to the page: unneeded p tags (for
// players) will be made invisible and default
// player names will be replaced with those submitted
// in the form

// restores the game to its default appearance
function refreshDefaults(){

  // draw the board  
  drawBoard();
  

  // show the player pieces on the start position on the board
  displayPlayer(1, initialPlayerSquare);
  displayPlayer(1, initialPlayerSquare);
  displayPlayer(2, initialPlayerSquare);
  displayPlayer(3, initialPlayerSquare);

  //set most of the menu and the form to be
  //invisible
  document.getElementById('playerForm').style.display='none';
  
  // set the players to all active
  //activePlayers = [true, true, true, true];

  // set current player to the first
  setCurrentPlayer(0);

  // set player names to deafult values
  //setPlayerName(0, "Player 1");
  //setPlayerName(1, "Player 2");
  //setPlayerName(2, "Player 3");
  //setPlayerName(3, "Player 4");

  // highlight the current player with the active color, and
  // make sure the other players are highlighted with the 
  // inactive color
  
  highlightPlayer(getCurrentPlayer(), getActiveColor());
  highlightPlayer(1, getInactiveColor());
  highlightPlayer(2, getInactiveColor());
  highlightPlayer(3, getInactiveColor());

}

// startNewGame()
// this function is used by the "New Game" button 
function startNewGame(){
  
  if (!isGameRunning) refreshDefaults();
  isGameRunning = true;

  // turn on display of form below game
  document.getElementById('playerForm').style.display='block';
  
}

// this function will show a player's piece at the square
// identified by its second argument
// Any previous display will be erased and set to its default
// thus, this function will need to access the current player's
// data and the board data, as well as the board context
function displayPlayer(playerNumber, squareNumber){
  
  var players = getPlayers();

  // calculate offsets for display
  var altx = boardSquares[squareNumber].x + players[playerNumber].width;
  var alty = boardSquares[squareNumber].y + players[playerNumber].depth;

  // draw the player's piece's image
  if (players[playerNumber].active){
    boardCtx.drawImage(players[playerNumber].getImg(), altx, alty, getImgFitX(), getImgFitY());
  
  // replace pixels at former location with 
  // the square's background color
  // so long as this is not a special square
  // Rules for special squares are unique:
  // redraw the entire square, and then redraw
  // active pieces still located there
  if (squareNumber != players[playerNumber].square){
 
    var altx2 = boardSquares[players[playerNumber].square].x + players[playerNumber].width;
    var alty2 = boardSquares[players[playerNumber].square].y + players[playerNumber].depth;

    boardCtx.beginPath();
    boardCtx.rect(altx2, alty2, getImgFitX(), getImgFitY()); 
    boardCtx.fillStyle = boardSquares[players[playerNumber].square].color; 
    boardCtx.fill();
  }
    
  // set the player's current location in the 
  // player data to squareNumber 
  setPlayerSquare(playerNumber, squareNumber);

  }
}

function getCardAndCompleteTurn(){
  
  // don't let players draw cards if a form is open!
  if(isPreventCardDraw || document.getElementById('playerForm').style.display != 'none'){
    return; 
  }

  isPreventCardDraw = true;

  // randomly select a card from the "deck"
  var cardName = processRandom(Math.floor(Math.random()*getProbabilityFactor()));
  console.log("This card: " + cardName);
  // send random number to display
  // undisplay previous card
  drawCard(cardName);

  // decide where piece should go
  var newPiecePosition = computeNewPosition(cardName, getCurrentPlayer());     
  // move the piece of the current player to new
  // position
  if(newPiecePosition > 0){
    displayPlayer(getCurrentPlayer(), newPiecePosition);
  }

  if (hasWon()){

    window.alert(getPlayers()[getCurrentPlayer()].name + " has won!");
    refreshDefaults();

    startNewGame();
  }

  else {
   
    // change gui and internal settings for next player
    setTimeout(switchTurn, delayInMillis);
    isPreventCardDraw = false;
  }

}

// modify this function when you change probabilityFactor
// or to adjust the probabilities of card draw
// be sure your probabilities add up to less than or
// equal to 1.0
function processRandom(randomIn){
  
  
  var factor = getProbabilityFactor();
  var cardArray = getCards();

  // chance of candycane is .025  
  var ccprob  = .025;
  // chance of molasses is .05 
  var molprob = .05;
  // chance of licorice is .075
  var licprob = .075;
  // chance of gumdrop is .1 
  var gumprob = .1;
  // chance of green card is zero --
  // just kidding, it's .125 
  var greenprob  = .125  
  // chance of purple is .125 
  var purpleprob = .125
  // chance of blue is .125 
  var blueprob   = .125
  // chance of yellow is .125 
  var yellowprob = .125
  // chance of orange is .125 
  var orangeprob = .125 
  // chance of red is whatever is left,
  // here that is approximately .125 
  // (there may be rounding errors
  // depending on your choice of
  // probabilityFactor, so the red
  // card's range of possible values
  // is effectively whatever is left).
  // A red probability is therefore
  // not directly put to use here
  
  // in the range of candycane? 
  if (randomIn < Math.floor(ccprob*factor)){
    return cardArray[8].name;
  }

  // in the range of molasses?
  if (randomIn < Math.floor((molprob + ccprob)*factor)){
    return cardArray[1].name;
  }

  // in the range of licorice? 
  if (randomIn < Math.floor((molprob+ccprob+licprob)*factor)){
    return cardArray[2].name;
  }
  
  // in the range of gumdrop?
  if (randomIn < Math.floor((molprob+ccprob+licprob+gumprob)*factor)){
    return cardArray[4].name;
  }

  // intermediate value to simplify the following
  var landprob = molprob+ccprob+licprob+gumprob;
  
  // CHANGE THIS AND THE FOLLOWING CONDITIONALS
  // if you want color cards to have distinct 
  // probabilities
  
  // in the range of a green card?
  if (randomIn < Math.floor((landprob+1*greenprob)*factor)){
    return cardArray[3].name;
  }

  // in the range of a blue card?
  if (randomIn < Math.floor((landprob+2*greenprob)*factor)){
    return cardArray[7].name;
  }

  // in the range of a yellow card?
  if (randomIn < Math.floor((landprob+3*greenprob)*factor)){
    return cardArray[6].name;
  }

  // in the range of an orange card?
  if (randomIn < Math.floor((landprob+4*greenprob)*factor)){
    return cardArray[0].name;
  }

  // in the range of a purple card?
  if (randomIn < Math.floor((landprob+5*greenprob)*factor)){
    return cardArray[5].name;
  }

  // otherwise, we are in range of red
  return cardArray[9].name;
}

// this function will loop through the squares after
// the square in which the current player's piece is
// located, looking for a match of the card name with 
// either the square color or the square type
function computeNewPosition(cardName, playerNumber){

  // goal: assign a new value to newSquare;
  // a value of -1 indicates no change in position
  var newSquare = -1;

  // get the players
  var players = getPlayers();

  // get the square of the player identified by playerNumber
  var currentSquare = players[playerNumber].square;

  // loop through contents of boardSquare after
  // the currentSquare
  var i = currentSquare + 1;
  while (i < boardSquares.length){

    // this comparison will probably need revision later
    if (cardName == boardSquares[i].color || cardName == boardSquares[i].type){
    
      newSquare = i;
      break;
    }

    i = i + 1;
  }
  

  return newSquare;
}

function switchTurn(){

  undrawCard();
  playerId = getCurrentPlayer();
  var players = getPlayers();
  var i = 1;
  while (!players[(playerId+i) % 4].active){
    //console.log("Id is " + (playerId+i)%4); 
    i = i+1;
  }
  highlightPlayer(playerId, getInactiveColor());
  highlightPlayer((playerId+i)%4, getActiveColor());
  setCurrentPlayer((playerId+i)%4);
}

function undrawCard(){

  // restore deck context to black background
  deckCtx.fillstyle = "#FF0000";
  deckCtx.fillRect(0,0,200,300);

}


function hasWon(){

  // determine whether the current player's piece
  // is at the end of the board

  return (boardSquares[getPlayers()[getCurrentPlayer()].square].type == "end");
}

function validateName(name){

  if (name == "" || name == null){
    
    return;
  }

  if(name.length > 12){
    
    window.alert("Names must be shorter than 12 characters");
    startNewGame();
  }

  if (name.search(/[^a-z\d\s:]/i) != -1){
  
    window.alert("Names may contain only letters");
    startNewGame();
  }

}


