// most candyland data is now located here
// this includes all images uploaded for use
// in the game, the playerData array, the
// boardSquares array, and miscellaneous state. 
// All accessors for this 
// data are also available in this file


/* convenient names for the presentation */

// change this to move players to different starting square
var initialPlayerSquare = 0;

// change this to affect the length of the delay before
// switching turn
var delayInMillis = 2000;

/* game related images below */

// the card image files
var img1 = new Image();
img1.src = "CandyLand/Orange.jpg";
var img2 = new Image();
img2.src = "CandyLand/Molasses.jpg";
var img3 = new Image();
img3.src = "CandyLand/Licorice.jpg";
var img4 = new Image();
img4.src = "CandyLand/Green.jpg";
var img5 = new Image();
img5.src = "CandyLand/Gumdrop.jpg";
var img6 = new Image();
img6.src = "CandyLand/Purple.jpg";
var img7 = new Image();
img7.src = "CandyLand/Yellow.jpg";
var img8 = new Image();
img8.src = "CandyLand/Blue.jpg";
var img9 = new Image();
img9.src = "CandyLand/Candy_Cane.jpg";
var img10 = new Image();
img10.src = "CandyLand/Red.jpg";

// board image file
var boardImg = new Image();
boardImg.src = "CandyLand/CandyLand_Board.png";

// gingerbread men image files
var piece1 = new Image();
piece1.src = "CandyLand/Blue_Gingerbread3.png";
var piece2 = new Image();
piece2.src = "CandyLand/Yellow_Gingerbread3.png";
var piece3 = new Image();
piece3.src = "CandyLand/Red_Gingerbread3.png";
var piece4 = new Image();
piece4.src = "CandyLand/Orange_Gingerbread3.png";


/* the following stores information about the players */

// the object containing the playerData and their locations
// getImg -- calls a function returning the image associated with that player
// active -- indicates whether this player has been registered to play
// name   -- the displayed name in the menu to the left of the board
// square -- counting from start at 0, the index of the square this player is
//        -> positioned at within the boardSquares array
// width  -- horizontal offset of this player's piece with respect to 
//        -> the coordinates of the top left corner of the square
// depth  -- vertical offset of this player's piece with respect to 
//        -> the coordinates of the top left corner of the square
var playerData = [{getImg: function(){ return piece1;}, 
                   active: true, 
                   name: "Player 1", 
                   square: initialPlayerSquare, 
                   width: 3, 
                   depth: 2
                   }, 
                  {getImg: function(){ return piece2;}, 
                   active: true, 
                   name: "Player 2", 
                   square: initialPlayerSquare, 
                   width: 16, 
                   depth: 2
                  }, 
                  {getImg: function(){ return piece3;}, 
                   active: true, 
                   name: "Player 3", 
                   square: initialPlayerSquare, 
                   width: 3, 
                   depth: 19
                  }, 
                  {getImg: function(){ return piece4;}, 
                   active: true, 
                   name: "Player 4", 
                   square: initialPlayerSquare, 
                   width: 16, 
                   depth: 19
                  }
                 ] 

function getPlayers(){

  return playerData;
}

function setPlayerSquare(playerNum, squareNum){

  playerData[playerNum].square = squareNum;
}

// size in pixels that the gingerbread men are limited
// to within each square
var imgFitX = 10;
var imgFitY = 16;

function getImgFitX(){
  return imgFitX;
}

function getImgFitY(){
  return imgFitY;
}

// highlight colors
var activeColor = "pink";
var inactiveColor = "powderblue";

function getActiveColor(){

  return activeColor;
}

function getInactiveColor(){

  return inactiveColor;
}

// currently active player; a number from 0-3,
// the actual range is not necessarily continuous
var currentPlayer = 0;

function getCurrentPlayer(){
  return currentPlayer;
}

function setCurrentPlayer(val){
  currentPlayer = val;
}

function setPlayerName(pnum, sname){

  var idNum = pnum + 1;
  playerData[pnum].name = sname;
  document.getElementById("p" + idNum + "Info").innerHTML = sname;
}

// playerId is a number in 0-3 that corresponds to an
// index of the activePlayers array
function highlightPlayer(playerId, toColor){
  
  var id = playerId + 1;

  document.getElementById("p" + id + "Info").style.backgroundColor = toColor; 
  
}
/* starting board data */

// boardSquares is an array containing all the data about the board in order
// from start of the game to end
// x,y   -- the coordinates in pixels of the top left corner of a square
// color -- the color associated with the square; used to find which square
//       -> to move to next, and in determining what to display after pieces 
//       -> have moved off a square
// type  -- also used in finding out where to go next; if "end", a player wins
var boardSquares = 
[{x: 63, y: 45, color: "gray", type: "start"}, // 0 start
 {x: 63, y: 8, color: "red", type: "color"},{x: 35, y: 8, color: "orange", type: "color"},          // 2
 {x: 4, y: 8, color: "green", type: "color"},{x: 4, y: 44, color: "blue", type: "color"},           // 4
 {x: 4, y: 80, color: "yellow", type: "color"},{x: 4, y: 115, color: "purple", type: "color"},      // 6
 {x: 4, y: 152, color: "red", type: "color"},{x: 34, y: 152, color: "orange", type: "color"},       // 8
 {x: 63, y: 153, color: "green", type: "color"},{x: 93, y: 154, color: "blue", type: "color"},      // 10
 {x: 123, y: 154, color: "yellow", type: "color"},{x: 123, y: 117, color: "purple", type: "color"},  
 {x: 123, y: 81, color: "red", type: "color"},{x: 123, y: 45, color: "orange", type: "color"},      
 {x: 123, y: 9, color: "green", type: "color"},{x: 153, y: 9, color: "blue", type: "color"},
 {x: 183, y: 9, color: "yellow", type: "color"},{x: 213, y: 9, color: "purple", type: "color"},
 {x: 243, y: 9, color: "red", type: "color"},{x: 273, y: 9, color: "orange", type: "color"},        // 20
 {x: 300, y: 9, color: "green", type: "color"},{x: 332, y: 9, color: "blue", type: "color"},
 {x: 360, y: 9, color: "yellow", type: "color"},{x: 390, y: 9, color: "purple", type: "color"},
 {x: 421, y: 9, color: "red", type: "color"},{x: 450, y: 9, color: "orange", type: "color"},
 {x: 480, y: 9, color: "green", type: "color"},{x: 510, y: 9, color: "blue", type: "color"},
 {x: 540, y: 9, color: "yellow", type: "color"},{x: 569, y: 9, color: "purple", type: "color"},     // 30
 {x: 569, y: 44, color: "red", type: "color"},{x: 569, y: 80, color: "pink", type: "gum" },         // gumdrop
 {x: 540, y: 80, color: "orange", type: "color"},{x: 509, y: 80, color: "green", type: "color"},                                                                                 //34  
 {x: 479, y: 80, color: "blue", type: "color"},{x: 450, y: 80, color: "yellow", type: "color"},
 {x: 419, y: 80, color: "purple", type: "color"},{x: 390, y: 80, color: "red", type: "color"},
 {x: 360, y: 80, color: "orange", type: "color"},{x: 330, y: 80, color: "green", type: "color"},    // 40
 {x: 300, y: 80, color: "blue", type: "color"},{x: 270, y: 80, color: "orange", type: "color"},
 {x: 243, y: 80, color: "yellow", type: "color"},{x: 212, y: 80, color: "purple", type: "color"},
 {x: 182, y: 80, color: "red", type: "color"},{x: 182, y: 117, color: "orange", type: "color"}, 
 {x: 182, y: 153, color: "green", type: "color"},{x: 213, y: 153, color: "blue", type: "color"},
 {x: 242, y: 153, color: "yellow", type: "color"},{x: 272, y: 153, color: "purple", type: "color"}, // 50
 {x: 301, y: 153, color: "red", type: "color"},{x: 331, y: 153, color: "orange", type: "color"}, 
 {x: 331, y: 191, color: "green", type: "color"},{x: 331, y: 225, color: "blue", type: "color"},
 {x: 301, y: 226, color: "yellow", type: "color"},{x: 271, y: 226, color: "purple", type: "color"},
 {x: 242, y: 226, color: "red", type: "color"},{x: 213, y: 226, color: "orange", type: "color"},
 {x: 182, y: 226, color: "green", type: "color"},{x: 153, y: 226, color: "blue", type: "color"},    // 60
 {x: 124, y: 226, color: "yellow", type: "color"},{x: 93, y: 226, color: "purple", type: "color"},
 {x: 64, y: 226, color: "red", type: "color"},{x: 34, y: 226, color: "orange", type: "color"}, 
 {x: 5, y: 226, color: "pink", type: "lic"},{x: 5, y: 263, color: "green", type: "color"},          // licorice
 {x: 5, y: 299, color: "blue", type: "color"},{x: 5, y: 334, color: "yellow", type: "color"},       
 {x: 5, y: 372, color: "purple", type: "color"},{x: 5, y: 409, color: "red", type: "color"},        // 70
 {x: 35, y: 409, color: "orange", type: "color"},{x: 64, y: 409, color: "green", type: "color"},
 {x: 94, y: 409, color: "blue", type: "color"},{x: 123, y: 409, color: "yellow", type: "color"}, 
 {x: 153, y: 409, color: "purple", type: "color"},{x: 183, y: 409, color: "red", type: "color"},
 {x: 212, y: 409, color: "orange", type: "color"},{x: 242, y: 409, color: "green", type: "color"},
 {x: 272, y: 409, color: "blue", type: "color"},{x: 272, y: 371, color: "yellow", type: "color"},   // 80
 {x: 272, y: 334, color: "purple", type: "color"},{x: 272, y: 300, color: "red", type: "color"},
 {x: 301, y: 300, color: "orange", type: "color"},{x: 330, y: 300, color: "green", type: "color"},
 {x: 360, y: 300, color: "blue", type: "color"},{x: 390, y: 300, color: "yellow", type: "color"}, 
 {x: 420, y: 300, color: "purple", type: "color"},{x: 450, y: 300, color: "red", type: "color"},
 {x: 480, y: 300, color: "orange", type: "color"},{x: 510, y: 300, color: "green", type: "color"},  // 90
 {x: 540, y: 300, color: "blue", type: "color"},{x: 570, y: 300, color: "yellow", type: "color"},
 {x: 570, y: 334, color: "purple", type: "color"},{x: 570, y: 370, color: "red", type: "color"},
 {x: 540, y: 370, color: "orange", type: "color"},{x: 510, y: 370, color: "green", type: "color"},
 {x: 480, y: 370, color: "blue", type: "color"},{x: 450, y: 370, color: "pink", type: "mol"},       // molasses
 {x: 420, y: 370, color: "yellow", type: "color"},{x: 390, y: 370, color: "purple", type: "color"}, // 100
 {x: 360, y: 370, color: "red", type: "color"},{x: 330, y: 370, color: "orange", type: "color"}, 
 {x: 330, y: 409, color: "green", type: "color"},{x: 330, y: 446, color: "blue", type: "color"},
 {x: 360, y: 446, color: "yellow", type: "color"},{x: 390, y: 446, color: "purple", type: "color"},
 {x: 420, y: 446, color: "red", type: "color"},{x: 450, y: 446, color: "orange", type: "color"}, 
 {x: 480, y: 446, color: "green", type: "color"},{x: 510, y: 446, color: "blue", type: "color"},    // 110
 {x: 540, y: 446, color: "yellow", type: "color"},{x: 570, y: 446, color: "purple", type: "color"},
 {x: 570, y: 482, color: "red", type: "color"},{x: 570, y: 520, color: "yellow", type: "color"}, 
 {x: 538, y: 520, color: "orange", type: "color"},{x: 508, y: 520, color: "green", type: "color"},
 {x: 478, y: 520, color: "blue", type: "color"},{x: 448, y: 520, color: "yellow", type: "color"},
 {x: 418, y: 520, color: "purple", type: "color"},{x: 390, y: 520, color: "red", type: "color"},    // 120
 {x: 360, y: 520, color: "orange", type: "color"},{x: 330, y: 520, color: "green", type: "color"},  
 {x: 300, y: 520, color: "blue", type: "color"},{x: 270, y: 520, color: "yellow", type: "color"},
 {x: 240, y: 520, color: "pink", type: "can"},{x: 210, y: 520, color: "purple", type: "color"},     // candy cane
 {x: 180, y: 520, color: "red", type: "color"},{x: 150, y: 520, color: "orange", type: "color"},
 {x: 120, y: 520, color: "green", type: "color"},{x: 90, y: 520, color: "blue", type: "color"},     // 130
 {x: 60, y: 520, color: "yellow", type: "color"},{x: 30, y: 520, color: "purple", type: "color"},
 {x: 4, y: 520, color: "red", type: "end"}                                                         // end
] 

/* following data relates to the deck */

// the deck of cards
// getImg -- returns the image associated with this card
// name   -- used in determining where a piece should go
var cardDeck = [{getImg: function(){ return img1;}, 
                 name: "orange"
                },
                {getImg: function(){ return img2;}, 
                 name: "mol"
                },
                {getImg: function(){ return img3;}, 
                 name: "lic"
                },
                {getImg: function(){ return img4;}, 
                 name: "green"
                },
                {getImg: function(){ return img5;}, 
                 name: "gum"
                },
                {getImg: function(){ return img6;}, 
                 name: "purple"
                },
                {getImg: function(){ return img7;}, 
                 name: "yellow"
                },
                {getImg: function(){ return img8;}, 
                 name: "blue"
                },
                {getImg: function(){ return img9;}, 
                 name: "can"
                },
                {getImg: function(){ return img10;}, 
                 name: "red"
                }
               ]

var deckLength = cardDeck.length;

// a number used to affect the chance of a particular card draw
// see processRandom() for more information
var probabilityFactor = 40;


function getProbabilityFactor(){
  return probabilityFactor;
}

function getCards(){
  return cardDeck;
}

/* miscellaneous game data */

// whether game is currently running
var isGameRunning = false;

var isPreventCardDraw = false;
